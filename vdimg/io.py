import numpy as np
import cv2
import math

from io import BytesIO
from PIL import Image

from vdimg import utils


@utils.with_fp(mode='rb')
def load_img(fp) -> np.ndarray:
    """
    Loads an image as a NumPy array

    :param fp: Either a file path, a file object or bytes

    :return: NumPy array representing the image
    """
    arr = Image.open(fp)
    arr = np.asarray(arr)
    arr = arr[:, :, [2, 1, 0]]
    return arr


@utils.with_fp(mode='rb')
def img_shape(fp):
    return Image.open(fp).size


def img2bytes(image: np.ndarray, format='.jpg'):
    _, buffer = cv2.imencode(format, image)
    with BytesIO(buffer) as f:
        return f.getvalue()
