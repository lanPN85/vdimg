install-dev:
	pip3 install --upgrade -e .[dev]

test:
	pytest tests/

test-coverage:
	pytest --cov=. tests
