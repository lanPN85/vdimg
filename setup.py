import os

from setuptools import setup, find_packages

setup(
    name='vdimg',
    description='VDSense image manipulation tools',
    version='0.1.2',
    url='',
    author='lanpn',
    author_email='lanpn@vdsense.com',
    classifiers=[
        'Programming Language :: Python :: 3.5'
    ],
    package_dir={'vdimg': 'vdimg'},
    packages=find_packages(exclude='tests'),
    python_requires='>=3.5, <4',
    install_requires=[
        'numpy', 'loguru', 'pybase64',
        'opencv-python==4.2.0.34', 'Pillow'
    ],
    extras_require={
        'dev': ['pytest', 'coverage']
    }
)
